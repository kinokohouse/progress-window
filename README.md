# Progress Window #
An AppleScriptable application for displaying a progress bar. Source available under an MIT license. There are many programs like this, and in many case it has been made obsolete by the progress indicator built in the current version of AppleScript, but this one has a couple of features that might come in handy. I also wanted something like this that I could distribute along with my own stuff, therefore I needed to roll my own.

The (somewhat) unique features are:

- Choice of one or two bars (so you could display the progress of processing, say, folders, as well as the processing of the files in the folders);
- Pollable Cancel button.

Just for the hell of it, I've also ported the app to AppleScriptObjC, because the prospect of using AppleScript to control an application written in AppleScript was simply too tantalising. :)

Although an example AppleScript is supplied (as compiled script and text) with examples of how everything works, please take the time also to look through the AppleScript dictionary for additional usage hints. A ready to use binary (compiled from the ObjC sources) for 10.7 and up is available from the downloads section.

The ASOC version is only available as source; there's been a change around 10.12 or 10.13 in how frames are treated in ASOC which is not backwards compatible (and the old way of treating them is not forward compatible), therefore some changes in the source are needed to get it to run on earlier versions of macOS.

Building
--------
Projects are for Xcode 9.4.1, but binary should run from 10.7 on.

Current version is 1.0 (build 1) for both versions.
