//
//  AppDelegate.h
//  Progress Window
//
//  Created by Petros Loukareas on 04-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

#pragma mark IBOutlets

@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *upperText;
@property (weak) IBOutlet NSProgressIndicator *upperBar;
@property (weak) IBOutlet NSTextField *lowerText;
@property (weak) IBOutlet NSProgressIndicator *lowerBar;


#pragma mark Scriptable Properties

@property (retain, nonatomic) NSString *upperLabel;
@property (nonatomic) double upperBarVal;
@property (nonatomic) double upperBarMax;
@property (nonatomic) BOOL upperBarInd;

@property (retain, nonatomic) NSString *lowerLabel;
@property (nonatomic) double lowerBarVal;
@property (nonatomic) double lowerBarMax;
@property (nonatomic) BOOL lowerBarInd;

@property (nonatomic) BOOL doubleBars;
@property (nonatomic) BOOL cancelFlag;


#pragma mark Exposed AppleScriptable Getters and Setters

#pragma mark ...for upper progress bar:

- (NSString *)upperLabel;
- (void)setUpperLabel:(NSString *)upperLabel;
- (double)upperBarVal;
- (void)setUpperBarVal:(double)upperBarVal;
- (double)upperBarMax;
- (void)setUpperBarMax:(double)upperBarMax;
- (BOOL)upperBarInd;
- (void)setUpperBarInd:(BOOL)upperBarInd;

#pragma mark ...for lower progress bar:

- (NSString *)lowerLabel;
- (void)setLowerLabel:(NSString *)lowerLabel;
- (double)lowerBarVal;
- (void)setLowerBarVal:(double)lowerBarVal;
- (double)lowerBarMax;
- (void)setLowerBarMax:(double)lowerBarMax;
- (BOOL)lowerBarInd;
- (void)setLowerBarInd:(BOOL)lowerBarInd;

#pragma mark ...for the control properties:

- (BOOL)doubleBars;
- (void)setDoubleBars:(BOOL)doubleBars;
- (BOOL)cancelFlag;
- (void)setCancelFlag:(BOOL)cancelFlag;

@end

