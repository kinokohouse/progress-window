//
//  AppDelegate.m
//  Progress Window
//
//  Created by Petros Loukareas on 04-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "AppDelegate.h"

#pragma mark Implementation

@implementation AppDelegate
@synthesize upperLabel = _upperLabel;
@synthesize upperBarVal = _upperBarVal;
@synthesize upperBarMax = _upperBarMax;
@synthesize upperBarInd = _upperBarInd;

@synthesize lowerLabel = _lowerLabel;
@synthesize lowerBarVal = _lowerBarVal;
@synthesize lowerBarMax = _lowerBarMax;
@synthesize lowerBarInd = _lowerBarInd;

@synthesize doubleBars = _doubleBars;
@synthesize cancelFlag = _cancelFlag;


#pragma mark App Delegate Methods

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    [self setUpperLabel:@"..."];
    [self setUpperBarVal:0.0];
    [self setUpperBarMax:1.0];
    [self setUpperBarInd:YES];
    [_upperBar setMinValue:0.0];
    
    [self setLowerLabel:@"..."];
    [self setLowerBarVal:0.0];
    [self setLowerBarMax:1.0];
    [self setLowerBarInd:YES];
    [_lowerBar setMinValue:0.0];
    
    [self setDoubleBars:NO];
    [self setCancelFlag:NO];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    if ([_window isVisible] == YES) {
        [_window orderOut:nil];
    }
}

- (BOOL)application:(NSApplication *)sender delegateHandlesKey:(NSString *)key {
    NSArray <NSString *> *keys = @[@"upperLabel", @"upperBarVal", @"upperBarMax", @"upperBarInd",
                                   @"lowerLabel", @"lowerBarVal", @"lowerBarMax", @"lowerBarInd",
                                   @"doubleBars", @"cancelFlag"];
    for (NSString *i in keys) {
        if ([i isEqualToString:key]) return YES;
    }
    return NO;
}


#pragma mark Getters and Setters

- (NSString *)upperLabel {
    return _upperLabel;
}

- (void)setUpperLabel:(NSString *)upperLabel {
    _upperLabel = upperLabel;
    [_upperText setStringValue:_upperLabel];
}

- (double)upperBarVal {
    return _upperBarVal;
}

- (void)setUpperBarVal:(double)upperBarVal {
    _upperBarVal = upperBarVal;
    [_upperBar setDoubleValue:_upperBarVal];
}

- (double)upperBarMax {
    return _upperBarMax;
}

- (void)setUpperBarMax:(double)upperBarMax {
    _upperBarMax = upperBarMax;
    [_upperBar setMaxValue:_upperBarMax];
}

- (BOOL)upperBarInd {
    return _upperBarInd;
}

- (void)setUpperBarInd:(BOOL)upperBarInd {
    _upperBarInd = upperBarInd;
    [_upperBar setIndeterminate:_upperBarInd];
}

- (NSString *)lowerLabel {
    return _lowerLabel;
}

- (void)setLowerLabel:(NSString *)lowerLabel {
    _lowerLabel = lowerLabel;
    [_lowerText setStringValue:_lowerLabel];
}

- (double)lowerBarVal {
    return _lowerBarVal;
}

- (void)setLowerBarVal:(double)lowerBarVal {
    _lowerBarVal = lowerBarVal;
    [_lowerBar setDoubleValue:_lowerBarVal];
}

- (double)lowerBarMax {
    return _lowerBarMax;
}

- (void)setLowerBarMax:(double)lowerBarMax {
    _lowerBarMax = lowerBarMax;
    [_lowerBar setMaxValue:_lowerBarMax];
}

- (BOOL)lowerBarInd {
    return _lowerBarInd;
}

- (void)setLowerBarInd:(BOOL)lowerBarInd {
    _lowerBarInd = lowerBarInd;
    [_lowerBar setIndeterminate:_lowerBarInd];
}

- (BOOL)doubleBars {
    return _doubleBars;
}

- (void)setDoubleBars:(BOOL)doubleBars {
    if (doubleBars == _doubleBars) return;
    _doubleBars = doubleBars;
    if (_doubleBars == NO) {
        NSRect frame = [_window frame];
        frame.origin.y += 46;
        frame.size.height = 127;
        [_lowerBar setHidden:YES];
        [_lowerText setHidden:YES];
        [_window setFrame:frame display:YES animate:NO];
    } else {
        NSRect frame = [_window frame];
        frame.origin.y -= 46;
        frame.size.height = 173;
        [_lowerBar setHidden:NO];
        [_lowerText setHidden:NO];
        [_window setFrame:frame display:YES animate:NO];
    }
}

- (BOOL)cancelFlag {
    return _cancelFlag;
}

- (void)setCancelFlag:(BOOL)cancelFlag {
    _cancelFlag = cancelFlag;
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self setCancelFlag:YES];
}

@end
