//
//  PWShowSingleBar.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWShowSingleBar.h"
#import "AppDelegate.h"

@interface PWShowSingleBar ()

@end

@implementation PWShowSingleBar

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [appDelegate setDoubleBars:NO];
    
    return nil;
}

@end

