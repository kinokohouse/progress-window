//
//  PWHideWindow.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWHideWindow.h"
#import "AppDelegate.h"

@interface PWHideWindow ()

@end

@implementation PWHideWindow

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [[appDelegate window] orderOut:nil];
    
    return nil;
}

@end

