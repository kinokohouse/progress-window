//
//  PWResetWindow.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWResetWindow_h
#define PWResetWindow_h


#endif /* PWResetWindow_h */

#import <Cocoa/Cocoa.h>

@interface PWResetWindow : NSScriptCommand

- (id)performDefaultImplementation;

@end

