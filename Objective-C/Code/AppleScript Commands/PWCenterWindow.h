//
//  PWCenterWindow.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWCenterWindow_h
#define PWCenterWindow_h


#endif /* PWCenterWindow_h */

#import <Cocoa/Cocoa.h>

@interface PWCenterWindow : NSScriptCommand

- (id)performDefaultImplementation;

@end

