//
//  PWFloatWindow.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWFloatWindow.h"
#import "AppDelegate.h"

@interface PWFloatWindow ()

@end

@implementation PWFloatWindow

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [[appDelegate window] setLevel:NSFloatingWindowLevel];
    
    return nil;
}

@end

