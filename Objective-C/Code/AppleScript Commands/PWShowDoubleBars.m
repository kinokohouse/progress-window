//
//  PWShowDoubleBars.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWShowDoubleBars.h"
#import "AppDelegate.h"

@interface PWShowDoubleBars ()

@end

@implementation PWShowDoubleBars

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [appDelegate setDoubleBars:YES];
    
    return nil;
}

@end

