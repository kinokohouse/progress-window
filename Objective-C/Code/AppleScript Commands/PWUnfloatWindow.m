//
//  PWUnfloatWindow.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWUnfloatWindow.h"
#import "AppDelegate.h"

@interface PWUnfloatWindow ()

@end

@implementation PWUnfloatWindow

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [[appDelegate window] setLevel:NSNormalWindowLevel];
    
    return nil;
}

@end

