//
//  PWShowDoubleBars.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWShowDoubleBars_h
#define PWShowDoubleBars_h


#endif /* PWShowDoubleBars_h */

#import <Cocoa/Cocoa.h>

@interface PWShowDoubleBars : NSScriptCommand

- (id)performDefaultImplementation;

@end

