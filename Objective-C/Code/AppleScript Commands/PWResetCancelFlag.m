//
//  PWResetCancelFlag.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWResetCancelFlag.h"
#import "AppDelegate.h"

@interface PWResetCancelFlag ()

@end

@implementation PWResetCancelFlag

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [appDelegate setCancelFlag:NO];
    
    return nil;
}

@end

