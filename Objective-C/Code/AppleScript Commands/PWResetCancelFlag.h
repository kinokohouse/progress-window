//
//  PWResetCancelFlag.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWResetCancelFlag_h
#define PWResetCancelFlag_h


#endif /* PWResetCancelFlag_h */

#import <Cocoa/Cocoa.h>

@interface PWResetCancelFlag : NSScriptCommand

- (id)performDefaultImplementation;

@end

