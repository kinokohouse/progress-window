//
//  PWShowWindow.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWShowWindow_h
#define PWShowWindow_h


#endif /* PWShowWindow_h */

#import <Cocoa/Cocoa.h>

@interface PWShowWindow : NSScriptCommand

- (id)performDefaultImplementation;

@end

