//
//  PWShowSingleBar.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWShowSingleBar_h
#define PWShowSingleBar_h


#endif /* PWShowSingleBar_h */

#import <Cocoa/Cocoa.h>

@interface PWShowSingleBar : NSScriptCommand

- (id)performDefaultImplementation;

@end

