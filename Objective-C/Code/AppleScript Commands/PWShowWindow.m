//
//  PWShowWindow.m
///  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWShowWindow.h"
#import "AppDelegate.h"

@interface PWShowWindow ()

@end

@implementation PWShowWindow

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [[appDelegate window] makeKeyAndOrderFront:nil];
    
    return nil;
}

@end

