//
//  PWUnfloatWindow.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWUnfloatWindow_h
#define PWUnfloatWindow_h


#endif /* PWUnfloatWindow_h */

#import <Cocoa/Cocoa.h>

@interface PWUnfloatWindow : NSScriptCommand

- (id)performDefaultImplementation;

@end

