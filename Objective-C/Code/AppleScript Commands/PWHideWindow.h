//
//  PWHideWindow.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWHideWindow_h
#define PWHideWindow_h


#endif /* PWHideWindow_h */

#import <Cocoa/Cocoa.h>

@interface PWHideWindow : NSScriptCommand

- (id)performDefaultImplementation;

@end

