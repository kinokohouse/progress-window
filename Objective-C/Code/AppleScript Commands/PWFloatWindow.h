//
//  PWFloatWindow.h
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#ifndef PWFloatWindow_h
#define PWFloatWindow_h


#endif /* PWFloatWindow_h */

#import <Cocoa/Cocoa.h>

@interface PWFloatWindow : NSScriptCommand

- (id)performDefaultImplementation;

@end

