//
//  PWCenterWindow.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWCenterWindow.h"
#import "AppDelegate.h"

@interface PWCenterWindow ()

@end

@implementation PWCenterWindow

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSScreen *screen = [NSScreen mainScreen];
    NSRect screenframe = [screen visibleFrame];
    NSRect windowframe = [[appDelegate window] frame];

    CGFloat windowheight = windowframe.size.height;
    CGFloat windowwidth = windowframe.size.width;
    
    CGFloat screenheight = screenframe.size.height;
    CGFloat screenwidth = screenframe.size.width;
    
    CGFloat newx = (screenwidth / 2) - (windowwidth / 2) + screenframe.origin.x;
    CGFloat newy = (screenheight / 2) - (windowheight / 2) + screenframe.origin.y;
    
    [[appDelegate window] setFrameOrigin:NSMakePoint(newx, newy)];
    
    return nil;
}

@end

