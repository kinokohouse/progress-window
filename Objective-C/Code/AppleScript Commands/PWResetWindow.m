//
//  PWResetWindow.m
//  Progress Window
//
//  Created by Petros Loukareas on 05-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "PWResetWindow.h"
#import "AppDelegate.h"

@interface PWResetWindow ()

@end

@implementation PWResetWindow

- (id)performDefaultImplementation {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];

    [appDelegate setUpperLabel:@"..."];
    [appDelegate setUpperBarVal:0.0];
    [appDelegate setUpperBarMax:1.0];
    [appDelegate setUpperBarInd:YES];

    [appDelegate setLowerLabel:@"..."];
    [appDelegate setLowerBarVal:0.0];
    [appDelegate setLowerBarMax:1.0];
    [appDelegate setLowerBarInd:YES];

    [appDelegate setDoubleBars:NO];
    [appDelegate setCancelFlag:NO];
    
    [[appDelegate window] setTitle:@"Progress Window"];
    
    return nil;
}

@end

