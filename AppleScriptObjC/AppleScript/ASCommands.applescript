--
--  ASCommands.applescript
--  Progress Window
--
--  Created by Petros Loukareas on 07-08-18.
--  Copyright © 2018 Kinoko House. All rights reserved.
--

script PWShowWindow
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's theWindow's makeKeyAndOrderFront_(missing value)
        return missing value
    end performDefaultImplementation
    
end script

script PWHideWindow
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's theWindow's orderOut_(missing value)
        return missing value
    end performDefaultImplementation
    
end script

script PWCenterWindow
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        set screen to current application's NSScreen's mainScreen
        set screenframe to screen's visibleFrame as list
        set windowframe to appDelegate's theWindow's frame()
        set windowwidth to item 1 of item 2 of windowframe as real
        set windowheight to item 2 of item 2 of windowframe as real
        set screenwidth to item 1 of item 2 of screenframe as real
        set screenheight to item 2 of item 2 of screenframe as real
        set newx to (screenwidth / 2) - (windowwidth / 2) + (item 1 of item 1 of screenframe as real)
        set newy to (screenheight / 2) - (windowheight / 2) + (item 2 of item 1 of screenframe as real)
        appDelegate's theWindow's setFrameOrigin_({newx, newy})
        return missing value
    end performDefaultImplementation
    
end script

script PWFloatWindow
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's theWindow's setLevel_(current application's NSFloatingWindowLevel)
        return missing value
    end performDefaultImplementation
    
end script

script PWUnfloatWindow
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's theWindow's setLevel_(current application's NSNormalWindowLevel)
        return missing value
    end performDefaultImplementation
    
end script

script PWResetWindow
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's setUpperLabel_("...")
        appDelegate's setUpperBarVal_(0.0 as real)
        appDelegate's setUpperBarMax_(1.0 as real)
        appDelegate's setUpperBarInd_(true)
        appDelegate's setLowerLabel_("...")
        appDelegate's setLowerBarVal_(0.0 as real)
        appDelegate's setLowerBarMax_(1.0 as real)
        appDelegate's setLowerBarInd_(true)
        appDelegate's setDoubleBars_(false)
        appDelegate's setCancelFlag_(false)
        appDelegate's theWindow's setTitle_("Progress Window")
        return missing value
    end performDefaultImplementation
    
end script

script PWShowDoubleBars
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's setDoubleBars_(true)
        return missing value
    end performDefaultImplementation
    
end script

script PWShowSingleBar
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's setDoubleBars_(false)
        return missing value
    end performDefaultImplementation
    
end script

script PWResetCancelFlag
    
    property parent: class "NSScriptCommand"
    
    on performDefaultImplementation()
        set appDelegate to current application's NSApp's delegate
        appDelegate's setCancelFlag_(false)
        return missing value
    end performDefaultImplementation
    
end script

