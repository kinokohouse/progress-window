--
--  AppDelegate.applescript
--  Progress Window
--
--  Created by Petros Loukareas on 07-08-18.
--  Copyright © 2018 Kinoko House. All rights reserved.
--

script AppDelegate
	property parent : class "NSObject"
	
    -- IBOutlets
    property theWindow : missing value
    property upperText : missing value
    property upperBar : missing value
    property lowerText : missing value
    property lowerBar : missing value
    
    -- AppleScriptable Properties
    property upperLabel : "..." as text
    property upperBarVal : 0.0 as real
    property upperBarMax : 1.0 as real
    property upperBarInd : true
    
    property lowerLabel : "..." as text
    property lowerBarVal : 0.0 as real
    property lowerBarMax : 1.0 as real
    property lowerBarInd : true
    
    property doubleBars : false
    property cancelFlag : false
    
	-- Application Delegate Methods
	on applicationWillFinishLaunching_(aNotification)
        upperBar's setMinValue_(0.0)
        lowerBar's setMinValue_(0.0)
    end applicationWillFinishLaunching_
	
	on applicationShouldTerminate_(sender)
        if theWindow's isVisible is true then theWindow's orderOut_(missing value)
    end applicationShouldTerminate_

    on application_delegateHandlesKey_(sender, key)
        return (key is in {"upperLabel", "upperBarVal", "upperBarMax", "upperBarInd", "lowerLabel", "lowerBarVal", "lowerBarMax", "lowerBarInd", "doubleBars", "cancelFlag"})
    end application_delegateHandlesKey_
    
    -- AppleScript handling
    on setUpperLabel_(newLabel)
        set upperLabel to newLabel
        upperText's setStringValue_(upperLabel)
    end setUpperLabel_
    
    on setUpperBarVal_(newValue)
        set upperBarVal to newValue
        upperBar's setDoubleValue_(upperBarVal)
    end setUpperBarVal_
    
    on setUpperBarMax_(newValue)
        set upperBarMax to newValue
        upperBar's setMaxValue_(upperBarMax)
    end setUpperBarMax_

    on setUpperBarInd_(newBool)
        set upperBarInd to newBool
        upperBar's setIndeterminate_(upperBarInd)
    end setUpperBarInd_

    on setLowerLabel_(newLabel)
        set lowerLabel to newLabel
        lowerText's setStringValue_(lowerLabel)
    end setLowerLabel_

    on setLowerBarVal_(newValue)
        set lowerBarVal to newValue
        lowerBar's setDoubleValue_(lowerBarVal)
    end setLowerBarVal_

    on setLowerBarMax_(newValue)
        set lowerBarMax to newValue
        lowerBar's setMaxValue_(lowerBarMax)
    end setLowerBarMax_

    on setLowerBarInd_(newBool)
        set lowerBarInd to newBool
        lowerBar's setIndeterminate_(lowerBarInd)
    end setLowerBarInd_
    
    on setDoubleBars_(newBool)
        if doubleBars is newBool then return
        set doubleBars to newBool
        set frame to theWindow's frame()
        if doubleBars is false then
            set orig_y to item 2 of item 1 of frame as real
            set orig_y to orig_y + 46
            set item 2 of item 1 of frame to orig_y
            set item 2 of item 2 of frame to 127
            lowerBar's setHidden_(true)
            lowerText's setHidden_(true)
            theWindow's setFrame_display_animate_(frame, true, false)
        else
            set orig_y to item 2 of item 1 of frame as real
            set orig_y to orig_y - 46
            set item 2 of item 1 of frame to orig_y
            set item 2 of item 2 of frame to 173
            lowerBar's setHidden_(false)
            lowerText's setHidden_(false)
            theWindow's setFrame_display_animate_(frame, true, false)
        end if
    end setDoubleBars_
    
    on setCancelFlag_(newBool)
        set cancelFlag to newBool
    end setCancelFlag_
    
    on cancelButtonPressed_(sender)
        setCancelFlag_(true)
    end cancelButtonPressed_

end script
