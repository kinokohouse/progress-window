//
//  main.m
//  Progress Window
//
//  Created by Petros Loukareas on 07-08-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AppleScriptObjC/AppleScriptObjC.h>

int main(int argc, const char * argv[]) {
    [[NSBundle mainBundle] loadAppleScriptObjectiveCScripts];
    return NSApplicationMain(argc, argv);
}
